using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace studlist.tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1_MAM()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {name = "Ivan", mark = 3},
                new Student {name = "Ivan", mark = 4},
                new Student {name = "Ivan", mark = 5},
                new Student {name = "Lyosha", mark = 3},
                new Student {name = "Sveta", mark = 5},
                new Student {name = "Sveta", mark = 5}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt = {"Sveta"};

            // Action
            var result = testList.MaxAverageMark();

            // Assert
            Assert.Equal(asrt, result);
        }

        [Fact]
        public void Test2_MAM()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {name = "Ivan", mark = 3},
                new Student {name = "Ivan", mark = 4},
                new Student {name = "Ivan", mark = 5},
                new Student {name = "Lyosha", mark = 5},
                new Student {name = "Sveta", mark = 5},
                new Student {name = "Sveta", mark = 5}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt = {"Sveta", "Lyosha"};
            Array.Sort(asrt);


            // Action
            var result = testList.MaxAverageMark();

            // Assert
            Assert.Equal(asrt, result);
        }
        
        [Fact]
        public void Test3_AM()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {exam = "Matan", mark = 3},
                new Student {exam = "Angem", mark = 4},
                new Student {exam = "Angem", mark = 5},
                new Student {exam = "TFKP", mark = 5},
                new Student {exam = "TFKP", mark = 5},
                new Student {exam = "TFKP", mark = 5}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt =
            {
                "Matan 3",
                "Angem 4,5",
                "TFKP 5"
            };
            Array.Sort(asrt);

            // Action
            var result = testList.AverageMark();

            // Assert
            Assert.Equal(asrt, result);
        }
        
        [Fact]
        public void Test4_BG()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {exam = "Matan", mark = 3, group = "SIB"},
                new Student {exam = "Matan", mark = 5, group = "SIB"},
                new Student {exam = "Matan", mark = 5, group = "SIB"},
                new Student {exam = "Angem", mark = 4, group = "SIB"},
                new Student {exam = "Angem", mark = 4, group = "SIB"},
                new Student {exam = "TFKP", mark = 4, group = "SIB"},
                new Student {exam = "TFKP", mark = 4, group = "SIB"},
                
                new Student {exam = "Matan", mark = 2, group = "SPB"},
                new Student {exam = "Matan", mark = 2, group = "SPB"},
                new Student {exam = "Angem", mark = 2, group = "SPB"},
                new Student {exam = "Angem", mark = 4, group = "SPB"},
                new Student {exam = "TFKP", mark = 2, group = "SPB"},
                new Student {exam = "TFKP", mark = 2, group = "SPB"}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt =
            {
                "Matan SIB 4,33",
                "Angem SIB 4",
                "TFKP SIB 4"
            };
            Array.Sort(asrt);

            // Action
            var result = testList.BestGroup();

            // Assert
            Assert.Equal(asrt, result);
        }

        [Fact]
        public void Test5_BG()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {exam = "Matan", mark = 3, group = "SIB"},
                new Student {exam = "Matan", mark = 5, group = "SIB"},
                new Student {exam = "Angem", mark = 5, group = "SIB"},
                new Student {exam = "Angem", mark = 5, group = "SIB"},
                new Student {exam = "TFKP", mark = 3, group = "SIB"},
                new Student {exam = "TFKP", mark = 4, group = "SIB"},
                
                new Student {exam = "Matan", mark = 3, group = "SPB"},
                new Student {exam = "Matan", mark = 5, group = "SPB"},
                new Student {exam = "Angem", mark = 5, group = "SPB"},
                new Student {exam = "Angem", mark = 5, group = "SPB"},
                new Student {exam = "TFKP", mark = 3, group = "SPB"},
                new Student {exam = "TFKP", mark = 2, group = "SPB"}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt =
            {
                "Matan SIB 4",
                "Matan SPB 4",
                "Angem SIB 5",
                "Angem SPB 5",
                "TFKP SIB 3,5"
            };
            Array.Sort(asrt);

            // Action
            var result = testList.BestGroup();

            // Assert
            Assert.Equal(asrt, result);
        }
        
        [Fact]
        public void Test6_BG()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {exam = "Matan", mark = 3, group = "SIB"},
                new Student {exam = "Angem", mark = 5, group = "SPB"}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt =
            {
                "Matan SIB 3",
                "Angem SPB 5"
            };
            Array.Sort(asrt);

            // Action
            var result = testList.BestGroup();

            // Assert
            Assert.Equal(asrt, result);
        }
        
        [Fact]
        public void Test7_MAM()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {name = "Ivan", mark = 5},
                new Student {name = "Ivan", mark = 3},
                new Student {name = "Ivan", mark = 5},
                new Student {name = "Lyosha", mark = 3},
                new Student {name = "Sveta", mark = 3},
                new Student {name = "Sveta", mark = 4}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt = {"Ivan"};

            // Action
            var result = testList.MaxAverageMark();

            // Assert
            Assert.Equal(asrt, result);
        }
        
        [Fact]
        public void Test8_AM()
        {
            // Arrange
            List<Student> studentsList = new List<Student>()
            {
                new Student {exam = "Matan", mark = 4},
                new Student {exam = "Angem", mark = 7},
                new Student {exam = "Angem", mark = 4},
                new Student {exam = "TFKP", mark = 5},
                new Student {exam = "TFKP", mark = 9},
                new Student {exam = "TFKP", mark = 5}
            };

            Class1 testList = new Class1();
            testList.students = studentsList;

            string[] asrt =
            {
                "Matan 4",
                "Angem 5,5",
                "TFKP 6,33"
            };
            Array.Sort(asrt);

            // Action
            var result = testList.AverageMark();

            // Assert
            Assert.Equal(asrt, result);
        }
    }
}
