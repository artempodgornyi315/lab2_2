﻿using System.Security.Principal;
using System;
using System.Collections.Generic;
using System.Linq;

namespace studlist
{
    public class Student
    {
        public string name, group, exam;
        public double mark;
    }

    public class Class1
    {
        public List<Student> students = new List<Student>();

        public IEnumerable<string> MaxAverageMark()
        {
            var groupByNameAvgMark = students
                .GroupBy(n => n.name, m => m.mark, (name, mark) => new {name, average = Math.Round(mark.Average() * 100) / 100});
            var maxMark = groupByNameAvgMark
                .Max(m => m.average);

            return groupByNameAvgMark.Where(s => s.average == maxMark).Select(s => s.name).OrderBy(s => s);
        }

        public IEnumerable<string> AverageMark()
        {
            var groupByExamAvgMark = students
                .GroupBy(e => e.exam, m => m.mark, (exam, mark) => new {exam, average = Math.Round(mark.Average() * 100) / 100})
                .OrderBy(s => s.exam);

            return groupByExamAvgMark.Select(s => $"{s.exam} {s.average}");
        }
        
        public IEnumerable<string> BestGroup()
        {
            var examGroups2 = students
                .GroupBy(s => new {s.exam, s.group, s.mark})
                .Select(s => new Student()
                {
                    exam = s.Key.exam,
                    group = s.Key.group,
                    mark = Math.Round(students
                        .Where(st => st.exam == s.Key.exam && st.group == s.Key.group)
                        .Select(st => st.mark)
                        .Average() * 100) / 100
                });

            return examGroups2
                .Where(
                    eg => eg.mark == examGroups2
                    .Where(egs => egs.exam == eg.exam)
                    .Select(am => am.mark)
                    .Max()
                    )
                .Select(eg => $"{eg.exam} {eg.group} {eg.mark.ToString()}")
                .Distinct()
                .OrderBy(ob => ob);
        }
    }
}
