﻿using System;

namespace sqsol
{
    public class SquareEquation
    {
        public const double e = 1e-12;
        public double[] solve(double a, double b, double c)
        {
            if (Math.Abs(a) < e) return new double[0];  // a != 0
            
            double d = b * b - 4 * a * c;            
            if (d < -e) return new double[0];    // При d < 0 корней нет

            if (d <= e)  // Один корень при d == 0
            {
                return new double[] {(-b) / (2 * a)};
            }

            if (d > e)  // Два корня при d > 0
            {
                return new double[] {(-b + Math.Sqrt(d)) / (2 * a), (-b - Math.Sqrt(d)) / (2 * a)};
            }
            else return new double[0];
        }
    }
}
