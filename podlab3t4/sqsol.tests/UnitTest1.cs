using System;
using Xunit;

namespace sqsol.tests
{
    public class UnitTest1
    {
        public const double e = 1e-12;
        [Fact]
        public void Test1()
        {
            // Arrange
            SquareEquation s = new SquareEquation();
            double[] asrt = {1};

            // Action
            double[] result = s.solve(1, -2, 1);

            // Assert
            Assert.True(Math.Abs(asrt[0] - result[0]) < e);
        }

        [Fact]
        public void Test2()
        {
            // Arrange
            SquareEquation s = new SquareEquation();
            double[] asrt = {-1, 1};

            // Action
            double[] result = s.solve(1, 0, -1);
            Array.Sort(result);

            // Assert
            Assert.True(Math.Abs(asrt[0] - result[0]) < e && Math.Abs(asrt[1] - result[1]) < e);
        }

        [Fact]
        public void Test3()
        {
            // Arrange
            SquareEquation s = new SquareEquation();
            double[] asrt = new double[0];

            // Action
            double[] result = s.solve(1, 0, 1);

            // Assert
            Assert.Equal(asrt, result);
        }

        [Fact]
        public void Test4()
        {
            // Arrange
            SquareEquation s = new SquareEquation();
            double[] asrt = new double[0];

            // Action
            double[] result = s.solve(0, 2, 1);

            // Assert
            Assert.Equal(asrt, result);
        }

        [Fact]
        public void Test5()
        {
            // Arrange
            SquareEquation s = new SquareEquation();
            double[] asrt = {-0.2, 1};

            // Action
            double[] result = s.solve(-5, 4, 1);

            // Assert
            Assert.True(Math.Abs(asrt[0] - result[0]) < e && Math.Abs(asrt[1] - result[1]) < e);
        }
    }
}
